﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Code
{
    [CreateAssetMenu]
    public class Level : ScriptableObject
    {
        public RecipeItem[] Ingredients;
        public float Time;
        public Sprite Background;
        public Sprite Player;
        public Sprite Demon;
        public GameObject[] Shelves;
		public AudioClip BGSound;
    }
}
