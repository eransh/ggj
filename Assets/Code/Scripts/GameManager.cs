﻿using UnityEngine;
using System.Collections;
using Assets.Code;
using System.Collections.Generic;
using System;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public static event Action<GameState> OnStateChanged;
    public static event Action<Level> OnLevelChanged;

    public static GameManager Instance
    {
        get
        {
            return m_instance;
        }
    }

    public bool IsLastLevel { get { return m_CurrLevelIndex == Levels.Length - 1; } }

    public SpriteRenderer RoomBackground;
    public SpriteRenderer PlayerRenderer;
    public Level[] Levels;

    private GameState m_State;
    public GameState State
    {
        get { return m_State; }
        private set
        {
            if (m_State != value)
            {
                m_State = value;
                if (OnStateChanged != null)
                {
                    OnStateChanged.Invoke(value);
                }
            }
        }
    }
    public float RemainingTime { get; private set; }

    private static GameManager m_instance;

    private Stack<RecipeItem> m_CurrRecipe;
    private int m_CurrLevelIndex;

    private Level m_CurrLevel;
    public Level CurrLevel
    {
        get { return m_CurrLevel; }
        set
        {
            m_CurrLevel = value;
            if (OnLevelChanged != null)
            {
                OnLevelChanged(value);
            }
        }
    }

    public RecipeItem[] CurrIngredients { get; private set; }

    void Awake()
    {
        m_instance = this;
    }

    void Update()
    {
        if (State == GameState.Running && CurrLevel.Time > 0)
        {
            CheckTime();
        }
    }

    public void OpenMainMenu()
    {
        RoomBackground.sprite = Levels[0].Background;
        PlayerRenderer.sprite = Levels[0].Player;
        State = GameState.Start;
    }

    public void StartGame()
    {
        LoadLevel(0);
        State = GameState.Running;
    }

    public void RecipeItemSelected(RecipeItem item)
    {
        int selectedItemID = item.Icon.GetInstanceID();
        int expectedItemID = m_CurrRecipe.Peek().Icon.GetInstanceID();
        if (selectedItemID == expectedItemID)
        {
            OnCorrectItemSelection();
        }
        else
        {
            OnIncorrectItemSelection();
        }
    }

    public void ReloadCurrentLevel()
    {
        LoadLevel(m_CurrLevelIndex);        
    }

    public void LoadNextLevel()
    {
        m_CurrLevelIndex++;
        if (m_CurrLevelIndex == Levels.Length)
        {
            State = GameState.End;
        }

        else
        {
            LoadLevel(m_CurrLevelIndex);
        }
    }

    private void LoadLevel(int index)
    {
        m_CurrLevelIndex = index;
        LoadIngredients(m_CurrLevelIndex);
        CurrLevel = Levels[m_CurrLevelIndex];
        RemainingTime = CurrLevel.Time;

        RoomBackground.sprite = CurrLevel.Background;
        PlayerRenderer.sprite = CurrLevel.Player;

		SoundManager.PlayBackgroundMusic(CurrLevel.BGSound);

        State = GameState.Running;
    }

    private void LoadIngredients(int levelIndex)
    {
        Level currLevel = Levels[levelIndex];
        int ingredientCount = currLevel.Ingredients.Length;
        CurrIngredients = new RecipeItem[ingredientCount];
        List<int> selectedValues = new List<int>(ingredientCount);
        for (int i = 0; i < ingredientCount; i++)
        {
            selectedValues.Add(i);
        }
        for (int i = 0; i < ingredientCount; i++)
        {
            int selectedValue = UnityEngine.Random.Range(0, selectedValues.Count);
            CurrIngredients[i] = currLevel.Ingredients[selectedValues[selectedValue]];
            selectedValues.RemoveAt(selectedValue);
        }
        m_CurrRecipe = new Stack<RecipeItem>(CurrIngredients.Reverse());
    }

    private void CheckTime()
    {
        RemainingTime -= Time.deltaTime;
        if (RemainingTime <= 0)
        {
            Debug.Log("You ran out of time");
            OnFailure();
        }
    }

    private void OnFailure()
    {
        State = GameState.Failure;        
    }

    private void OnLevelCompleted()
    {
        State = GameState.Success;   
    }

    private void OnIncorrectItemSelection()
    {
        Debug.Log("Fail");
        OnFailure();
    }

    private void OnCorrectItemSelection()
    {
        m_CurrRecipe.Pop();
        if (m_CurrRecipe.Count == 0)
        {
            OnRecipeCompleted();
        }
    }

    private void OnRecipeCompleted()
    {
        OnLevelCompleted();
    }

    public enum GameState
    {
        Start,
        End,
        Running,
        Failure,
        Success
    }
}
