﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelItemsLoader : MonoBehaviour {

	public Transform[] SpawnPoints;
    [HideInInspector]
	public List<RecipeItem> RecipeItems;

	void Awake()
	{
		RecipeItems = new List<RecipeItem>();
	}

	void OnEnable()
	{
		GameManager.OnStateChanged += GameManager_Instance_OnStateChanged;
		GameManager.OnLevelChanged += GameManager_OnLevelChanged;
	}

	void OnDisable()
	{
		GameManager.OnStateChanged -= GameManager_Instance_OnStateChanged;
		GameManager.OnLevelChanged -= GameManager_OnLevelChanged;
	}

	void GameManager_Instance_OnStateChanged (GameManager.GameState obj)
	{
		if (obj == GameManager.GameState.Failure)
		{
			ClearStage();
		}
	}

	void GameManager_OnLevelChanged (Assets.Code.Level obj)
	{
		ClearStage();
        int ingredientsCount = GameManager.Instance.CurrLevel.Ingredients.Length;
        List<int> selectedValues = new List<int>();
        for (int i=0;i<SpawnPoints.Length; i++)
        {
            selectedValues.Add(i);           
        }
		for(int i=0; i< ingredientsCount; i++)
		{
            int selectedValue = Random.Range(0, selectedValues.Count);    
			RecipeItem recipeItem = Instantiate(GameManager.Instance.CurrLevel.Ingredients[i]) as RecipeItem;
			recipeItem.transform.SetParent(SpawnPoints[selectedValues[selectedValue]], false);
			RecipeItems.Add(recipeItem);
            selectedValues.RemoveAt(selectedValue);

        }
	}

	void ClearStage()
	{
		for(int i=RecipeItems.Count-1; i>=0; i--)
		{
			Destroy(RecipeItems[i].gameObject);
		}
		RecipeItems.Clear();
	}
}
