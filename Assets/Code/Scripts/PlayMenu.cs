﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayMenu : MonoBehaviour {

	public Image[] ItemsIcon;
	private EndMenu endMenu;

	public GameObject TimerObject;
	public RectTransform TimerBarFromt;
	public Image TimerBarFrontImage;

	void Start()
	{
		endMenu = transform.parent.GetComponentInChildren<EndMenu>(true);
	}

	void OnEnable()
	{
		SetBookForLevel();
		GameManager.OnStateChanged += GameManager_Instance_OnStateChanged;
		GameManager.OnLevelChanged += GameManager_OnLevelChanged;
	}

	void OnDisable()
	{
		GameManager.OnStateChanged -= GameManager_Instance_OnStateChanged;
		GameManager.OnLevelChanged -= GameManager_OnLevelChanged;
	}

	void Update()
	{
		if (GameManager.Instance.CurrLevel.Time > 0)
		{
			float remainingBar = GameManager.Instance.RemainingTime / GameManager.Instance.CurrLevel.Time;
			//TimerBarFromt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, remainingBar*600f);
			TimerBarFrontImage.fillAmount = remainingBar;
		}
	}

	void GameManager_Instance_OnStateChanged (GameManager.GameState obj)
	{
		if (obj == GameManager.GameState.End)
		{
			endMenu.gameObject.SetActive(true);
			gameObject.SetActive(false);
		}
	}

	void GameManager_OnLevelChanged (Assets.Code.Level obj)
	{
		SetBookForLevel();
	}

	void ClearBook()
	{
		for(int i=0; i<ItemsIcon.Length; i++)
		{
			ItemsIcon[i].gameObject.SetActive(false);
		}
	}

	void SetBookForLevel()
	{
		ClearBook();
		for(int i=0; i<GameManager.Instance.CurrLevel.Ingredients.Length; i++)
		{
			ItemsIcon[i].gameObject.SetActive(true);
			ItemsIcon[i].sprite = GameManager.Instance.CurrIngredients[i].Icon;
			Vector3 size = GameManager.Instance.CurrIngredients[i].Icon.bounds.size;
			float scale = size.x / size.y;
			if (size.y>size.x)
			{
				//ItemsIcon[i].rectTransform.sizeDelta = new Vector2(scale * size.x, size.y);
				ItemsIcon[i].rectTransform.sizeDelta = new Vector2(100f * scale, 100f);
			}else{
				scale = size.y / size.x;
				ItemsIcon[i].rectTransform.sizeDelta = new Vector2(100f, 100f * scale);
			}
        }
		if(GameManager.Instance.CurrLevel.Time > 0)
		{
			TimerObject.SetActive(true);
		}else{
			TimerObject.SetActive(false);
		}
	}
}
