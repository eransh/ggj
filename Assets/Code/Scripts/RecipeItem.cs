﻿using UnityEngine;
using System.Collections;

public class RecipeItem : MonoBehaviour
{
    public Sprite Icon;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

	public void OnItemClicked()
	{
		GameManager.Instance.RecipeItemSelected(this);
		gameObject.SetActive(false);
	}
}
