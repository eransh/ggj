﻿using UnityEngine;
using System.Collections;

public class StartMenu : MonoBehaviour {

	private PlayMenu playMenu;

	void Start()
	{
		playMenu = transform.parent.GetComponentInChildren<PlayMenu>(true);
	}

	void OnEnable()
	{
		GameManager.OnStateChanged += GameManager_Instance_OnStateChanged;
	}

	void OnDisable()
	{
		GameManager.OnStateChanged -= GameManager_Instance_OnStateChanged;
	}

	void GameManager_Instance_OnStateChanged (GameManager.GameState obj)
	{
		if (obj == GameManager.GameState.Running)
		{
			playMenu.gameObject.SetActive(true);
			gameObject.SetActive(false);
		}
	}

	public void PlayClicked()
	{
		GameManager.Instance.StartGame();
	}
}
