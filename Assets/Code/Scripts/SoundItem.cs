﻿using UnityEngine;
using System.Collections;

public class SoundItem : MonoBehaviour {

	public AudioSource ItemAudioSource;

	void Start()
	{
		StartCoroutine(StopSFX());
	}

	void OnDisable()
	{
		Destroy(gameObject);
	}

	IEnumerator StopSFX()
	{
		yield return new WaitForSeconds(ItemAudioSource.clip.length);
		while (ItemAudioSource.isPlaying)
		{
			yield return null;
		}
		gameObject.SetActive(false);
	}
}
