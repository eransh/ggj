﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public static SoundManager Instance;
	public AudioSource BackgroundMusicSource;
	public SoundItem SoundItemPrefab;

	void Awake()
	{
		Instance = this;
	}

	public static void PlayBackgroundMusic(AudioClip audioClip)
	{
		float curTime = Instance.BackgroundMusicSource.time;
		Instance.BackgroundMusicSource.clip = audioClip;
		Instance.BackgroundMusicSource.Play();
		Instance.BackgroundMusicSource.time = curTime;
	}

	public static void PlaySFX(AudioClip audioClip)
	{
		if (audioClip)
		{
			SoundItem soundItem = Instantiate(Instance.SoundItemPrefab) as SoundItem;
			soundItem.ItemAudioSource.clip = audioClip;
			soundItem.ItemAudioSource.Play();
			Instance.BackgroundMusicSource.clip = null;
		}
	}


}
