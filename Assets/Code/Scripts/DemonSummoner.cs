﻿using UnityEngine;
using System.Collections;

public class DemonSummoner : MonoBehaviour
{
    [SerializeField]
    float m_TimeToShowDemon;
    [SerializeField]
    SpriteRenderer m_DemonRenderer;
    [SerializeField]
    Animation m_Animation;
    [SerializeField]
	ParticleSystem m_BunnyParticleSystem;
	[SerializeField]
	AudioClip m_SFX;
	[SerializeField]
	AudioClip m_SFXBunnies;

    // Use this for initialization
    void Start()
    {
        GameManager.OnStateChanged += GameManager_OnStateChanged;
    }

    private void GameManager_OnStateChanged(GameManager.GameState state)
    {
        if (state == GameManager.GameState.Success)
        {
            m_DemonRenderer.sprite = GameManager.Instance.CurrLevel.Demon;
            m_DemonRenderer.enabled = true;
			m_Animation.Play();
			SoundManager.PlaySFX(m_SFX);
            if (GameManager.Instance.IsLastLevel)
            {
                Invoke("LoadFinalAnimation", m_TimeToShowDemon);
            }
            else
            {
                Invoke("LoadNextLevel", m_TimeToShowDemon);
            }
        }
        else
        {
            m_DemonRenderer.enabled = false;
            m_Animation.Stop();
        }
    }
    private void LoadFinalAnimation()
    {
		m_BunnyParticleSystem.Play();
		SoundManager.PlaySFX(m_SFXBunnies);
        Invoke("LoadNextLevel", m_TimeToShowDemon);
    }
    private void LoadNextLevel()
    {
        m_BunnyParticleSystem.Stop();
        GameManager.Instance.LoadNextLevel();       
    }

    void OnDisable()
    {
        GameManager.OnStateChanged -= GameManager_OnStateChanged;
    }
}
