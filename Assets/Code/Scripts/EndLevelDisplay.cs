﻿using UnityEngine;
using System.Collections;
using System;

public class EndLevelDisplay : MonoBehaviour
{
    [SerializeField]    float FailureSequenceTimeInSeconds;
	[SerializeField]    ParticleSystem m_BunnyParticleSystem;

	[SerializeField]
	AudioClip m_SFX;

    void Start()
    {
        GameManager.OnStateChanged += GameManager_OnStateChanged;
        m_BunnyParticleSystem.startLifetime = FailureSequenceTimeInSeconds;
    }

    private void GameManager_OnStateChanged(GameManager.GameState state)
    {
        switch (state)
        {
            case GameManager.GameState.Failure:
                StartFailureSequence();
                break;
            default:
                m_BunnyParticleSystem.Stop();
                m_BunnyParticleSystem.gameObject.SetActive(false);             
                break;
        }
    }

    private void StartFailureSequence()
    {
        m_BunnyParticleSystem.gameObject.SetActive(true);
        m_BunnyParticleSystem.Play();
		SoundManager.PlaySFX(m_SFX);
        Invoke("ReloadCurrentLevel", FailureSequenceTimeInSeconds);
    }

    private void ReloadCurrentLevel()
    {
        GameManager.Instance.ReloadCurrentLevel();
    }

    void OnDisable()
    {
        GameManager.OnStateChanged -= GameManager_OnStateChanged;
    }
}
