﻿using UnityEngine;
using System.Collections;

public class EndMenu : MonoBehaviour {

	private StartMenu startMenu;

	void Start()
	{
		startMenu = transform.parent.GetComponentInChildren<StartMenu>(true);
	}

	void OnEnable()
	{
		GameManager.OnStateChanged += GameManager_Instance_OnStateChanged;
		SoundManager.PlayBackgroundMusic(GameManager.Instance.Levels[GameManager.Instance.Levels.Length-1].BGSound);
	}

	void OnDisable()
	{
		GameManager.OnStateChanged -= GameManager_Instance_OnStateChanged;
	}

	void GameManager_Instance_OnStateChanged (GameManager.GameState obj)
	{
		if (obj == GameManager.GameState.Start)
		{
			startMenu.gameObject.SetActive(true);
			SoundManager.PlayBackgroundMusic(GameManager.Instance.Levels[0].BGSound);
			gameObject.SetActive(false);
		}
	}

	public void OpenStartMenu()
	{
		GameManager.Instance.OpenMainMenu();
	}
}
